const fs = require("fs");

const inputs = fs.readFileSync("input.txt").toString().split("\r\n");

// Puzzle 1
let freq = 0;
inputs.forEach(elem => {
    if (elem[0] == '-') {
        freq -= parseInt(elem.slice(1));
    } else {
        freq += parseInt(elem.slice(1));
    }
});
console.log("Secret frequency is " + freq);

// Puzzle 2
let frequenciesMet = [], run = true;
freq = 0;
while (run) {
    for (let j = 0; j < inputs.length; j++) {
        if (inputs[j][0] == '-') {
            freq -= parseInt(inputs[j].slice(1));
        } else {
            freq += parseInt(inputs[j].slice(1));
        }

        if (frequenciesMet.includes(freq)) {
            console.log("First repeat frequency is " + freq);
            run = false;
            break;
        }
        frequenciesMet.push(freq);
    }
}