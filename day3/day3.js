const fs = require("fs");

const inputs = fs.readFileSync("input.txt").toString().split("\r\n");

let coords = [];
let maxX = 0;
let maxY = 0;
let overlaps = 0;
let magicSquare = "";
inputs.forEach(element => {
    const words = element.split(" ");
    const startPos = words[2].split(",");
    const size = words[3].split("x");
    startPos[1] = startPos[1].replace(":", "");

    let coord = {
        id: words[0].slice(1),
        corner: {
            x: parseInt(startPos[0]),
            y: parseInt(startPos[1])
        },
        size: {
            x: parseInt(size[0]),
            y: parseInt(size[1])
        },
        overlaps: false
    };
    coords.push(coord);

    // Find edge coordinates
    if (coord.corner.x + coord.size.x > maxX) {
        maxX = coord.corner.x + coord.size.x;
    }

    if (coord.corner.y + coord.size.y > maxY) {
        maxY = coord.corner.y + coord.size.y;
    }
});

const IsWithinSquare = (square, coord) => {
    if (coord.x <= square.corner.x + square.size.x &&
        coord.y <= square.corner.y + square.size.y) {
        return true;
    }
    return false;
};

for (let x = 0; x < maxX; x++) {
    for (let y = 0; y < maxY; y++) {
        let check = {
            x: x,
            y: y
        };
        // Optimization: leave out coordinates that are not near the area being checked
        const validSquares = coords.filter(coord => coord.corner.x < x && coord.corner.y < y);

        const overlappingSquares = validSquares.filter(coord => IsWithinSquare(coord, check));
        if (overlappingSquares.length > 1) {
            overlaps++;
            overlappingSquares.forEach(square => square.overlaps = true);
        }
    }
}
magicSquare = coords.find(square => !square.overlaps).id;
console.log("Total overlaps:", overlaps, "magic square id:", magicSquare);