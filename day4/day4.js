const fs = require("fs");

const inputs = fs.readFileSync("input.txt").toString().split("\r\n");

let data = inputs.map(item => {
    const words = item.split(" ");
    let dateitems = words[0].slice(1).replace(/-/g, " ").split(" ");
    let timeitems = words[1].slice(0, 5).replace(/:/, " ").split(" ");

    let date = new Date(
        parseInt(dateitems[0]),
        parseInt(dateitems[1]) - 1,
        parseInt(dateitems[2]),
        parseInt(timeitems[0]),
        parseInt(timeitems[1])
    );
    let action;
    switch (words[2]) {
        case "wakes":
            action = "wake";
            break;
        case "falls":
            action = "sleep";
            break;
        default: // Action is guard id to signify start of duty
            action = words[3].slice(1);
            break;
    }
    return {
        action: action,
        timestamp: date
    };
});

// Order by date & time
data = data.sort((a, b) => {
    if (a.timestamp.getTime() < b.timestamp.getTime()) {
        return -1;
    }
    return 1;
});

// Refine list of actions into list of guards and accumulate the sleeping
// minutes to the correct guards
let guards = [];
let lastGuard;
for (let i = 0; i < data.length; i++) {
    const item = data[i];

    if (item.action === "sleep") {
        // sleep action: do nothing

    } else if (item.action === "wake") {
        // Guard wakes up: calculate the minutes he spent asleep
        let currentGuard = guards.find(guard => guard.id === lastGuard);
        // Find last action and calculate delta
        const amount = item.timestamp.getMinutes() - data[i - 1].timestamp.getMinutes();
        currentGuard.total += amount;
        // Accumulate the minutes to the guard's data
        for (let j = data[i - 1].timestamp.getMinutes(); j < item.timestamp.getMinutes(); j++) {
            if (currentGuard.minutes.hasOwnProperty(j)) {
                currentGuard.minutes[j]++;
            } else {
                currentGuard.minutes[j] = 1;
            }
        }
    } else { // Guard on duty changes
        lastGuard = parseInt(item.action);

        if (!guards.find(guard => guard.id === lastGuard)) {
            guards.push({ id: lastGuard, total: 0, minutes: {} });
        }
    }
}

// Sort unique guards by total amount slept
// This is used for puzzle 1 solution
guards = guards.sort((a, b) => {
    if (a.total < b.total) {
        return 1;
    }
    return -1;
});

// Find the minute and guard that was most commonly asleep
let highestAmount = 0;
let highestIndex;
let chosenGuard;
guards.forEach(guard => {
    for(let minute in guard.minutes) {
        if (guard.minutes[minute] > highestAmount) {
            highestAmount = guard.minutes[minute];
            chosenGuard = guard;
            highestIndex = minute;
        }
    }
});

console.log(chosenGuard.id, highestIndex);