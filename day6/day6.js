const fs = require("fs");

const inputs = fs.readFileSync("input.txt").toString().split("\r\n");

const coords = inputs.map(item => {
    const coord = item.split(", ");
    return { x: parseInt(coord[0]), y: parseInt(coord[1]), infinite: false, areaSize: 0 };
});

let gridMinX = Infinity;
let gridMaxX = -Infinity;
let gridMinY = Infinity;
let gridMaxY = -Infinity;

// Find extreme coordinates
for (let key in coords) {
    if (coords[key].x > gridMaxX) {
        gridMaxX = coords[key].x;
    }
    if (coords[key].x < gridMinX) {
        gridMinX = coords[key].x;
    }
    if (coords[key].y > gridMaxY) {
        gridMaxY = coords[key].y;
    }
    if (coords[key].y < gridMinY) {
        gridMinY = coords[key].y;
    }
}

const FindClosestCoord = (coord) => {
    let closest;
    let closestDistance = Infinity;

    for (let key in coords) {
        const distance = Math.abs(coord.x - coords[key].x)
            + Math.abs(coord.y - coords[key].y);
        if (distance < closestDistance) {
            closest = coords[key];
            closestDistance = distance;
        } else if (distance === closestDistance) {
            // Distance can be same to two different nodes -> return undef
            closest = undefined;
        }
    }

    return closest;
};

const FindLargestArea = () => {
    let biggestArea = 0;
    let biggestCoord;
    for (let key in coords) {
        if (!coords[key].infinite
            && coords[key].areaSize > biggestArea) {
            biggestArea = coords[key].areaSize;
            biggestCoord = coords[key];
        }
    }
    return biggestCoord;
};

// Find infinite coordinate origins: map out the edge regions
// and see which would continue to infinity beyond the edge

// Top row infinites
for (let i = gridMinX - 1; i < gridMaxX + 1; i++) {
    const coord = FindClosestCoord({ x: i, y: gridMinY - 1 });
    if (coord) {
        coord.infinite = true;
    }
}
// Bottom row infinites
for (let i = gridMinX - 1; i < gridMaxX + 1; i++) {
    const coord = FindClosestCoord({ x: i, y: gridMaxY + 1 });
    if (coord) {
        coord.infinite = true;
    }
}
// Left row infinites
for (let i = gridMinY - 1; i < gridMaxY + 1; i++) {
    const coord = FindClosestCoord({ x: gridMinX - 1, y: i });
    if (coord) {
        coord.infinite = true;
    }
}
// Right row infinites
for (let i = gridMinY - 1; i < gridMaxY + 1; i++) {
    const coord = FindClosestCoord({ x: gridMaxX + 1, y: i });
    if (coord) {
        coord.infinite = true;
    }
}

for (let x = gridMinX; x < gridMaxX; x++) {
    for (let y = gridMinY; y < gridMaxY; y++) {
        let found = FindClosestCoord({ x: x, y: y });
        if (found && !found.infinite) {
            found.areaSize++;
        }
    }
}

console.log(FindLargestArea());

const DistanceToCoord = (start, target) => {
    return Math.abs(start.x - target.x)
        + Math.abs(start.y - target.y);
};

let regionSize = 0;
for (let x = gridMinX; x < gridMaxX; x++) {
    for (let y = gridMinY; y < gridMaxY; y++) {
        let sumDistance = 0;
        for (let key in coords) {
            sumDistance += DistanceToCoord(
                { x: x, y: y }, coords[key]
            );
            // Optimization: stop checking coord if
            // limit distance is broken
            if (sumDistance >= 10000) {
                break;
            }
        }
        if (sumDistance < 10000) {
            regionSize++;
        }
    }
}

console.log("Size of area w/ <10000 distance to each node is",regionSize);