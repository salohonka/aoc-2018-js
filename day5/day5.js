const fs = require("fs");

const input = fs.readFileSync("input.txt").toString();

const HasReactions = string => {
    for (let i = 0; i < string.length; i++) {
        if (string[i].match(/[a-z]/)
            && string[i].toUpperCase() === string[i + 1]) {
            return i;
        } else if (string[i].match(/[A-Z]/)
            && string[i].toLowerCase() === string[i + 1]) {
            return i;
        }
    }
    return false;
}

// Use regex to find and remove all reactive combinations
const React = string => {
    let polymer = string;
    let regex;
    let reaction = HasReactions(polymer);
    while (reaction) {
        regex = new RegExp(
            `${polymer[reaction].toLowerCase()}` +
            `${polymer[reaction].toUpperCase()}|` +
            `${polymer[reaction].toUpperCase()}` +
            `${polymer[reaction].toLowerCase()}`,
            "g"
        );
        polymer = polymer.replace(regex, "");
        reaction = HasReactions(polymer);
    }
    return polymer;
}

// Find all unique letters in the polymer and test the optimal
// letter to remove
let uniques = [];
let regex;
let shortestLength = input.length;
let shortestCharacter;
for (let i = 0; i < input.length; i++) {
    const element = input[i].toLowerCase();
    if (!uniques.find(item => item === element)) {
        uniques.push(element);

        polymer = input;
        regex = new RegExp(`[${element}${element.toUpperCase()}]`, 'g');
        polymer = polymer.replace(regex, "");
        polymer = React(polymer);
        if (polymer.length < shortestLength) {
            shortestLength = polymer.length;
            shortestCharacter = element;
        }
    }
}
console.log("shortest length is", shortestLength, "after removing", shortestCharacter);