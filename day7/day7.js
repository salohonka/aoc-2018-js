const fs = require("fs");

const inputs = fs.readFileSync("input.txt").toString().split("\r\n");

const steps = inputs.map(item => {
    return {
        complete: item.split(" ")[1],
        allows: item.split(" ")[7]
    }
});

let uniqueSteps = [];
let doneSteps = [];

const FindOrCreateUniqueStep = id => {
    let step = uniqueSteps.find(item => item.id === id);
    if (!step) {
        step = { id: id, required: [] };
        uniqueSteps.push(step);
    }
    return step;
}

// Map out unique steps and their requirements
for (let key in steps) {
    const allowed = steps[key].allows;
    const required = steps[key].complete;

    FindOrCreateUniqueStep(required);
    let step = FindOrCreateUniqueStep(allowed);
    step.required.push(steps[key].complete);
}

const IdSort = (a, b) => {
    if (a.id < b.id) return -1;
    return 1;
}

// Loop until all steps are complete
let available = [];
do {
    // Sort to ensure alphabetical completion
    uniqueSteps = uniqueSteps.sort(IdSort);
    // Find steps that can be completed based on previous steps
    available = uniqueSteps.filter(item => {
        return item.required.every(required => doneSteps.find(item => item === required));
    });
    // Mark task as complete and remove from available pool
    if (available.length > 0) {
        doneSteps.push(available[0].id);
        uniqueSteps.splice(uniqueSteps.findIndex(item => item === available[0]), 1);
    }
} while (available.length > 0)

console.log("order of completion", doneSteps.join(""));

// Puzzle 2 not complete