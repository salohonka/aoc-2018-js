const fs = require("fs");

const inputs = fs.readFileSync("input.txt").toString().split("\r\n");

// Puzzle 1
let twos = 0, threes = 0;
inputs.forEach(item => {
    let twoFound = false, threeFound = false, letters = item.split("");
    for (let i = 0; i < letters.length; i++) {
        const matchCount = letters.filter(sample => sample === letters[i]).length;
        if (matchCount == 2) {
            twoFound = true;
        } else if (matchCount == 3) {
            threeFound = true;
        }
    }
    if (twoFound) {
        twos++;
    }
    if (threeFound) {
        threes++;
    }
});
console.log(`Matchsum is ${twos} * ${threes} = ${twos * threes}`);

// Puzzle 2
const DifferenceCount = (string1, string2) => {
    let diff = 0;
    for (let i = 0; i < string1.length; i++) {
        if (string1[i] !== string2[i]) {
            diff++;
        }
    }
    return diff;
};

const SameLetters = (string1, string2) => {
    let letters = [];
    for (let i = 0; i < string1.length; i++) {
        if (string1[i] == string2[i]) {
            letters.push(string1[i]);
        }
    }
    return letters.join("");
}

let stop = false;
for (let i = 0; i < inputs.length; i++) {
    for (let j = 0; j < inputs.length; j++) {
        if (i == j) {
            continue;
        }
        if (DifferenceCount(inputs[i], inputs[j]) == 1) {
            console.log("Same letters are", SameLetters(inputs[i], inputs[j]));
            stop = true;
            break;
        }
    }
    if (stop) {
        break;
    }
}