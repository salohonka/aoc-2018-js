## Advent of Code 2018: A JS implementation
Each task is in its own folder, some days may print only the last puzzles solution.

### How to run
1. Get Node.js
2. Navigate into a folder for a day
3. Run `node day#.js`, where # is replaced by the day of the folder you are in

#### Matias Salohonka, 2018/2019